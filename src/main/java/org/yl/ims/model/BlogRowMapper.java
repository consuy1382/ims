package org.yl.ims.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.yl.ims.model.db.Blog;

public class BlogRowMapper extends BaseRowMapper<Blog> {

	@Override
	public Blog mapRow(ResultSet arg0, int arg1) throws SQLException {
		return (Blog) super.setEntityProperties(Blog.class, arg0);
	}

}
