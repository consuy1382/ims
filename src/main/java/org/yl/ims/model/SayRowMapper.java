package org.yl.ims.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.yl.ims.model.db.Say;

public class SayRowMapper extends BaseRowMapper<Say> {

	@Override
	public Say mapRow(ResultSet rs, int rowNum) throws SQLException {
		return (Say) super.setEntityProperties(Say.class, rs);
	}

}
