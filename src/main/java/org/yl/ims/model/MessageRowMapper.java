package org.yl.ims.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.yl.ims.model.db.MessageInfo;

public class MessageRowMapper extends BaseRowMapper<MessageInfo> {

	@Override
	public MessageInfo mapRow(ResultSet arg0, int arg1) throws SQLException {
		return (MessageInfo) super.setEntityProperties(MessageInfo.class, arg0);
	}

}
