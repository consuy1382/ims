package org.yl.ims.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.yl.ims.model.db.UserInfo;

public class UserRowMapper extends BaseRowMapper<UserInfo> {

	@Override
	public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
		return (UserInfo) super.setEntityProperties(UserInfo.class, rs);
	}

}
