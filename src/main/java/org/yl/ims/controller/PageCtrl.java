package org.yl.ims.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.yl.ims.service.UserService;
import org.yl.ims.util.PathUtil;

@Controller
public class PageCtrl {
	@Autowired
	private UserService userService;

	@RequestMapping(value = { "guest", "guest/*" })
	public String index(HttpServletRequest request, ModelMap map) {
		map.put("rootPath", PathUtil.getRelatedRootPath(request));
		map.put("imsPath", PathUtil.getServletRelatedPath(request));
		return "page/default";
	}

	@RequestMapping(value = { "admin","admin/*"  })
	public String admin(HttpServletRequest request, ModelMap map) {
		map.put("rootPath", PathUtil.getRelatedRootPath(request));
		map.put("imsPath", PathUtil.getServletRelatedPath(request));
		return "page/manager-default";
	}
	
	@RequestMapping(value = { "say","say/*"  })
	public String say(HttpServletRequest request, ModelMap map) {
		map.put("rootPath", PathUtil.getRelatedRootPath(request));
		map.put("imsPath", PathUtil.getServletRelatedPath(request));
		return "page/say-time";
	}
}
