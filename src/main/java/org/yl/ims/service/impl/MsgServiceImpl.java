package org.yl.ims.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yl.ims.model.MessageRowMapper;
import org.yl.ims.model.db.MessageInfo;
import org.yl.ims.service.MsgService;
import org.yl.ims.util.Page;
import org.yl.ims.util.dbUtil.ObjectedJDBCTemplate;

@Service
public class MsgServiceImpl implements MsgService {

	@Autowired
	private ObjectedJDBCTemplate template;

	@Override
	public Page<MessageInfo> getMsgData(int pageNumber, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageInfo getMessageInfoById(Integer blogId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer delMessageInfoById(Integer blogId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer saveOrUpdateMessageInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MessageInfo getMessageInfoByStatus(Integer status) {
		String sql = "SELECT * FROM MESSAGEINFO WHERE STATUS = :status";
		Map<String, Integer> p = new HashMap<String, Integer>();
		p.put("status", status);
		List<MessageInfo> msgList = template.query(sql, p,
				new MessageRowMapper());

		return msgList.size() > 0 ? msgList.get(0) : null;
	}

}
