package org.yl.ims.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.yl.ims.model.SayRowMapper;
import org.yl.ims.model.db.Say;
import org.yl.ims.service.SayService;
import org.yl.ims.util.Page;
import org.yl.ims.util.dbUtil.PagingSqlBuilder;

@Service
public class SayServiceImpl extends BaseService<Say> implements SayService {

	@Override
	public Page<Say> getMsgData(int pageNumber, int pageSize) {
		String rawSql = "SELECT * FROM SHUOSHUO ORDER BY CREATETIME DESC ";
		return this.paging(rawSql, pageSize, pageNumber, new SayRowMapper());
	}

	@Override
	public Say getSayById(Integer blogId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer delBSayById(Integer blogId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer saveOrUpdateSay() {
		// TODO Auto-generated method stub
		return null;
	}

}
