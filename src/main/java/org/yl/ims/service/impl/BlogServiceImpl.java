package org.yl.ims.service.impl;

import org.springframework.stereotype.Service;
import org.yl.ims.model.BlogRowMapper;
import org.yl.ims.model.db.Blog;
import org.yl.ims.service.BlogService;
import org.yl.ims.util.Page;

@Service
public class BlogServiceImpl extends BaseService<Blog> implements BlogService {

	@Override
	public Page<Blog> getBlogData(int pageNumber, int pageSize) {
		String rawSql = "select * from blog order by createTime desc";
		return this.paging(rawSql, pageSize, pageNumber, new BlogRowMapper());
	}

	@Override
	public Blog getBlogById(Integer blogId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer delBlogById(Integer blogId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer saveOrUpdateBlog() {
		// TODO Auto-generated method stub
		return null;
	}

}
