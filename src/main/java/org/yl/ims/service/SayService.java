package org.yl.ims.service;

import org.yl.ims.model.db.Say;
import org.yl.ims.util.Page;

public interface SayService {
	public Page<Say> getMsgData(int pageNumber, int pageSize);

	public Say getSayById(Integer blogId);

	public Integer delBSayById(Integer blogId);

	public Integer saveOrUpdateSay();
}
